import java.util.Scanner;

public class PatratPerfect {

	public static void main(String[] args)
	{
		int ok=1;
		do {
			
			Scanner in = new Scanner(System.in);
			System.out.print("! Aceasta aplicatie testeaza daca un numar natural citit de la tastatura este patrat perfect !\n");
			System.out.print("Introduceti un numar natural ( >=0 ) \n");
		
			System.out.print("Numarul citit = ");
			int a = in.nextInt();
		
			if(a>=0)
			{
				double sqrt = Math.sqrt(a);
				int sqrtint = (int) sqrt;
			
				if(Math.pow(sqrt,2) == Math.pow(sqrtint,2))
				{
					System.out.print("Numarul " + a + " este patrat perfect\n");
				}
				
				else
			
				{
					System.out.print("Numarul " + a + " citit nu este patrat perfect\n");
				}
			}
			
			else
			
			{
				System.out.print("Numarul " + a + " nu este natural. Va rugam introduceti un numar >=0\n");
			}
		
			System.out.print("Doriti sa testati alt numar?   Introduceti 1 pentru a testa alt numar sau 0 pentru a opri programul\n");
			ok=in.nextInt();
		}while(ok==1);
	}
}



